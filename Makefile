.PHONY: install
install:
	install -d $(DESTDIR)/usr/lib/systemd/system
	install -m 644 systemd/hkiosk-set-network@.service $(DESTDIR)/usr/lib/systemd/system/hkiosk-set-network@.service
	install -m 644 systemd/hkiosk-set-network@.path $(DESTDIR)/usr/lib/systemd/system/hkiosk-set-network@.path
	install -m 644 systemd/hkiosk-reload-network.service $(DESTDIR)/usr/lib/systemd/system/hkiosk-reload-network.service
	install -m 644 systemd/hkiosk-reload-network.path $(DESTDIR)/usr/lib/systemd/system/hkiosk-reload-network.path
	install -d $(DESTDIR)/usr/libexec
	install -m 755 libexec/hkiosk-set-network $(DESTDIR)/usr/libexec/hkiosk-set-network

.PHONY: uninstall
uninstall:
	-rm $(DESTDIR)/usr/lib/systemd/system/hkiosk-set-network@.service
	-rm $(DESTDIR)/usr/lib/systemd/system/hkiosk-set-network@.path
	-rm $(DESTDIR)/usr/lib/systemd/system/hkiosk-reload-network.service
	-rm $(DESTDIR)/usr/lib/systemd/system/hkiosk-reload-network.path
	-rm $(DESTDIR)/usr/libexec/hkiosk-set-network
