hKiosk Set Hostname Service
===========================

[![MIT license][license-image]][license-link]

A simple service that sets the hostname of the system to the value of the `/media/.set/hostname` file, if it exists. 
The file is automatically removed after the hostname is set.

Installation
------------

```shell
DESTDIR= make install
```

Usage
-----

The `/media/.set/hostname` file should contain the variable `SET_HOSTNAME` with the desired hostname.

```shell
SET_HOSTNAME=my-new-hostname
```

License
-------

[MIT][license-link]


[license-image]: https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square
[license-link]: LICENSE
