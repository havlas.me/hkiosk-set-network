#!/bin/sh
# SPDX-License-Identifier: MIT
# Copyright (C) 2024, Tomáš Havlas <tomas@havlas.me>
# Distributed under the MIT License.
#
# This file is part of the hkiosk-set-network project.
# https://gitlab.com/havlas.me/hkiosk-set-network

set -e -u -o pipefail

if [ $# -ne 1 ]; then
    cat >&2 <<EOHELP
Usage: $( basename "$0" ) INTERFACE

Environment:
  NETWORK_MACADDRESS the MAC address to be set
  NETWORK_MTU      the MTU to be set
  NETWORK_MULTICAST whether multicast is enabled
  NETWORK_POLICY   the activation policy
  NETWORK_REQUIRED  whether the network is required for online

  NETWORK_AUTOCONF whether to use DHCP
  NETWORK4_ADDRESS  the IPv4 address to be set
  NETWORK4_GATEWAY  the IPv4 gateway to be set
  NETWORK4_NAMESERVER the IPv4 nameserver to be set
  NETWORK6_ADDRESS  the IPv6 address to be set
  NETWORK6_GATEWAY  the IPv6 gateway to be set
  NETWORK6_NAMESERVER   the IPv6 nameserver to be set
  NETWORK_DNSSEC   whether DNSSEC is enabled
  NETWORK_NTP=          time synchronization server address

  WPA_SSID              SSID of the network to connect to
  WPA_PSK               password to be used when connecting to the network
  WPA_HIDDEN=           whether the network is hidden
EOHELP
    exit 2
fi

INTERFACE="$1"; shift

if [ "${DODESTROY:-}" = "DESTROY" ]; then
    rm -f "/etc/systemd/network/00-${INTERFACE}.network"
    rm -f "$( readlink "/etc/iwd/${INTERFACE}.current" )"
    rm -f "/etc/iwd/${INTERFACE}.current"
    exit 0
fi

NETWORKFILE="$( mktemp -t "${INTERFACE}.networkd.XXXXXXXX" )"
[ -z "${WPA_SSID:-}" ] || \
IWDFILE="$( mktemp -t "${INTERFACE}.iwd.XXXXXXXX" )"

cleanup() {
    rm -f "$NETWORKFILE"
    [ -z "${IWDFILE:-}" ] || \
    rm -f "$IWDFILE"
}
trap cleanup EXIT

{
  echo "[Match]"
  echo "Name=$INTERFACE"
  [ -z "${WPA_SSID:-}" ] || \
  echo "SSID=$WPA_SSID"
  echo
  echo "[Link]"
  [ -z "${NETWORK_MACADDRESS:-}" ] || \
  echo "MACAddress=$NETWORK_MACADDRESS"
  [ -z "${NETWORK_MTU:-}" ] || \
  echo "MTUBytes=$NETWORK_MTU"
  echo "Multicast=${NETWORK_MULTICAST-no}"
  echo "ActivationPolicy=${NETWORK_POLICY:-up}"
  [ -z "${NETWORK_REQUIRED:-}" ] || \
  echo "RequiredForOnline=$NETWORK_REQUIRED"
  echo
  echo "[Network]"
  [ -z "${NETWORK_AUTOCONF:-}" ] || \
  echo "DHCP=$NETWORK_AUTOCONF"
  [ -z "${NETWORK4_ADDRESS:-}" ] || \
  echo "Address=$NETWORK4_ADDRESS"
  [ -z "${NETWORK4_GATEWAY:-}" ] || \
  echo "Gateway=$NETWORK4_GATEWAY"
  [ -z "${NETWORK4_NAMESERVER:-}" ] || \
  echo "DNS=$NETWORK4_NAMESERVER"
  [ -z "${NETWORK6_ADDRESS:-}" ] || \
  echo "Address=$NETWORK6_ADDRESS"
  [ -z "${NETWORK6_GATEWAY:-}" ] || \
  echo "Gateway=$NETWORK6_GATEWAY"
  [ -z "${NETWORK6_NAMESERVER:-}" ] || \
  echo "DNS=$NETWORK6_NAMESERVER"
  [ -z "${NETWORK_DNSSEC:-}" ] || \
  echo "DNSSEC=$NETWORK_DNSSEC"
  [ -z "${NETWORK_NTP:-}" ] || \
  echo "NTP=$NETWORK_NTP"
} > "$NETWORKFILE"

[ -z "${WPA_SSID:-}" ] || \
{
  echo "[Settings]"
  [ -z "${WPA_HIDDEN:-}" ] || \
  echo "Hidden=$WPA_HIDDEN"
  echo
  echo "[Security]"
  echo "Passphrase=$WPA_PSK"
} > "$IWDFILE"

rm -f "$( readlink "/etc/iwd/${INTERFACE}.current" )"
rm -f "/etc/iwd/${INTERFACE}.current"
if [ -n "${WPA_SSID:-}" ]; then
    install -m 600 "$IWDFILE" "/var/lib/iwd/${WPA_SSID}.psk"
    ln -snf "/var/lib/iwd/${WPA_SSID}.psk" "/etc/iwd/${INTERFACE}.current"
fi
install -m 644 "$NETWORKFILE" "/etc/systemd/network/00-${INTERFACE}.network"
